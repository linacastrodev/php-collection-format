<!-- nav -->
<div class="pos-f-t">
  <div class="collapse" id="navbarToggleExternalContent">
    <div class="bg-light p-4" style="background-color: #CCD1D1">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active">				
					<a class="nav-link" href="https://gitlab.com/linacastrodev" style="color: #fff"><i class="pe-7s-id" style="font-size: 25px"></i>&nbsp;&nbsp;&nbsp;<label style="color: #fff;margin-bottom: 0px;margin-top: 14.5px;"><b>Go to Gitlab</label></b><span class="sr-only">(current)</span></a>
				</li>
			</ul>
    </div>
  </div>
  <nav class="navbar navbar-light" style="background-color: #EAECEE">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation" style="border: none;width: 20%;">
    <img src="../assets/img/logo-coin.png" class="img-responsive">
    </button>
  </nav>
<!-- FIN nav -->
</div>
<style>
.img-responsive{
	width: 60%; 
	height: 65px;
	float: left;
}
@media only screen and (max-width: 375px) {
	.img-responsive {
	  width: 300%;
	}
  }
</style>
