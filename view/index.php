<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <title>Collection account Format </title>
  <link rel="stylesheet" type="text/css" href="../assets/inicio.css">
  <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../assets/ionicons/css/ionicons.min.css">
  <link rel="stylesheet" type="text/css" href="../assets/js/bootstrap.min.js">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
</head>
<body>
<!-- START NAV -->
<?php
include '../view/nav.php';
?>
<!-- END NAV -->
<!-- START JUMBOTRON -->
  <div class="jumbotron">
    <div class="container">
      <h1 class="display-3 text-center">Collection account format.</h1>
      <p class="lead text-center">¡Welcome! in this section yours can create a format for the account collection <br> to all contractos of company COIN S.A.S</p>
    </div>
  </div>
<!-- END JUMBOTRON -->
<!-- START FORM CONTRACT -->
<div class="container-fluid">
  <div class="row">
      <div class="col-12 col-md-12">
        <h4 class="text-center">Name of contractor </h4>
      </div>
      <div class="col-6 col-md-4" style="margin-left: 33%;margin-top: 25px">
        <div class="input-group">
          <input type="text" class="form-control" name="busqueda" id="busqueda" placeholder="Please fill the name" aria-describedby="basic-addon1">
        </div>
      </div>
      <br>
      <div id="resultado" class="col-md-12" style="margin-top: 30px;"></div>
  </div>
</div>
<!-- END FORM CONTRACT -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.2.3/jquery-confirm.min.css"><script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.2.3/jquery-confirm.min.js"></script>
<script type="text/javascript" src="../assets/datatables/jquery.dataTables.js"></script>
<script type="text/javascript" src="../assets/js/bootstrap.min.js"></script>
<!-- START JQUERY -->
<script type="text/javascript">
  $(document).ready(function(){
    var consulta;
    $("#busqueda").focus();
    $("#busqueda").keyup(function(e){
      consulta = $("#busqueda").val();
      $.ajax({
        type: "POST",
        url: "../controllers/inicio.php",
        data: "b="+consulta,
        dataType: "html",
        beforeSend: function()
        {
          $("#resultado").html("<p align='center'><img src='lol.gif' style='width:35%; height='10px' /></p>");
        },
        error: function()
        {
          alert("error petición ajax");
        },
        success: function(data){
          $("#resultado").empty();
          $("#resultado").append(data);
        }
      });
    });
  });
</script>
<!-- END JQUERY -->
<!-- START FOOTER -->
<?php
include '../view/footer.php';
?>
<!-- END FOOTER -->
</body>
</html>
