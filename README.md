# PHP COLLECTION ACCOUNT FORMAT!

_This project is a website where people who show the contractos about the company COIN S.A.S. This proyect made around the year 2016 when PHP begin with OOP and Laravel 5.5 (this proyect is create with OOP PHP, JQUERY, AJAX AND MYSQL)

## Installation and initialization 🔧

_If you have already cloned the project code, you must install:_

```
XAMPP 
```

_To start the project in developer mode use the script:_

```
RUN XAMPP SERVICE
```

## Built with 🛠️

- [Php](https://www.php.net/)
- [Jquery](https://jquery.com/)
- [AJAX](https://www.w3schools.com/xml/ajax_intro.asp)
- [HTML5](https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/HTML5)
- [CSS3](https://developer.mozilla.org/en-US/docs/Archive/CSS3) 
- [MYSQL](https://www.mysql.com/)

## Authors ✒️

- **Lina Castro** - _Frontend and backend Developer_ - [lirrums](https://gitlab.com/linacastrodev)

## License 📄

This project is under the License (MIT)

## Acknowledgements 🎁

- We greatly appreciate to the company for motivating us with the project. providing us with tools and their knowledge for our professional growth📢
